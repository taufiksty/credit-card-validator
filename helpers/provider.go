package helpers

var CreditCardProviders = map[string]string{
	"^4":      "Visa",
	"^5[1-5]": "Mastercard",
	"^3[47]":  "American Express",
	// Add more card provider patterns as needed
}
