package exception

import (
	"net/http"

	"github.com/go-playground/validator/v10"
	"gitlab.com/taufiksty/credit-card-validator/helpers"
	"gitlab.com/taufiksty/credit-card-validator/models/web"
)

func ErrorHandler(writer http.ResponseWriter, request *http.Request, err interface{}) {
	if notFoundError(writer, request, err) {
		return
	}

	if clientError(writer, request, err) {
		return
	}

	internalServerError(writer, request, err)
}

func clientError(writer http.ResponseWriter, _, err interface{}) bool {
	exception, ok := err.(validator.ValidationErrors)

	if ok {
		writer.Header().Set("Content-Type", "application/json")
		writer.WriteHeader(http.StatusBadRequest)

		webResponse := web.WebResponse{
			Code:   http.StatusBadRequest,
			Status: "BAD REQUEST",
			Data:   exception.Error(),
		}

		helpers.WriteToResponseBody(writer, webResponse)

		return true
	} else if exception, ok := err.(ClientError); ok {
		writer.Header().Set("Content-Type", "application/json")
		writer.WriteHeader(http.StatusBadRequest)

		webResponse := web.WebResponse{
			Code:   http.StatusBadRequest,
			Status: "BAD REQUEST",
			Data: web.CreditCardValidateResponse{
				CardNumber: exception.CardNumber,
				Provider:   "Unknown",
				Validate:   false,
				Message:    exception.Error,
			},
		}

		helpers.WriteToResponseBody(writer, webResponse)

		return true
	} else {
		return false
	}
}

func internalServerError(writer http.ResponseWriter, _, err interface{}) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusInternalServerError)

	webResponse := web.WebResponse{
		Code:   http.StatusInternalServerError,
		Status: "INTERNAL SERVER ERROR",
		Data:   err,
	}

	helpers.WriteToResponseBody(writer, webResponse)
}

func notFoundError(writer http.ResponseWriter, _, err interface{}) bool {
	exception, ok := err.(NotFoundError)
	if ok {
		writer.Header().Set("Content-Type", "application/json")
		writer.WriteHeader(http.StatusNotFound)

		webResponse := web.WebResponse{
			Code:   http.StatusNotFound,
			Status: "NOT FOUND",
			Data:   exception.Error,
		}

		helpers.WriteToResponseBody(writer, webResponse)

		return true
	} else {
		return false
	}
}
