package app

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/taufiksty/credit-card-validator/controllers"
	"gitlab.com/taufiksty/credit-card-validator/exception"
)

func NewRouter(creditCardController controllers.CreditCardController) *httprouter.Router {
	router := httprouter.New()

	router.GET("/api/credit-cards/:cardNumber", creditCardController.ValidateCreditCard)

	router.PanicHandler = exception.ErrorHandler
	router.NotFound = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		exception.ErrorHandler(w, r, exception.NewNotFoundError("Page not found"))
	})

	return router
}
