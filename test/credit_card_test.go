package test

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
	"gitlab.com/taufiksty/credit-card-validator/app"
	"gitlab.com/taufiksty/credit-card-validator/controllers"
	"gitlab.com/taufiksty/credit-card-validator/models/web"
	"gitlab.com/taufiksty/credit-card-validator/services"
)

func setupRouter() http.Handler {
	validate := validator.New()
	creditCardService := services.NewCreditCardService(validate)
	creditCardController := controllers.NewCreditCardController(creditCardService)

	return app.NewRouter(creditCardController)
}

type TestCaseValidate struct {
	CardNumber string                         `json:"card_number"`
	Code       int                            `json:"code"`
	Status     string                         `json:"status"`
	Data       web.CreditCardValidateResponse `json:"data"`
}

func TestValidateCreditCard(t *testing.T) {
	testCases := []TestCaseValidate{
		{CardNumber: "4111111111111111", Code: 200, Status: "OK", Data: web.CreditCardValidateResponse{Provider: "Visa", Validate: true, Message: "Credit card number is valid"}},
		{CardNumber: "5500000000000004", Code: 200, Status: "OK", Data: web.CreditCardValidateResponse{Provider: "Mastercard", Validate: true, Message: "Credit card number is valid"}},
		{CardNumber: "340000000000009", Code: 200, Status: "OK", Data: web.CreditCardValidateResponse{Provider: "American Express", Validate: true, Message: "Credit card number is valid"}},
		{CardNumber: "6011000000000004", Code: 200, Status: "OK", Data: web.CreditCardValidateResponse{Provider: "Unknown", Validate: true, Message: "Credit card number is valid"}},
		{CardNumber: "3530111333300000", Code: 200, Status: "OK", Data: web.CreditCardValidateResponse{Provider: "Unknown", Validate: true, Message: "Credit card number is valid"}},
		{CardNumber: "123456789012345", Code: 400, Status: "BAD REQUEST", Data: web.CreditCardValidateResponse{Provider: "Unknown", Validate: false, Message: "Credit card number is invalid"}},
		{CardNumber: "4111111111111112", Code: 400, Status: "BAD REQUEST", Data: web.CreditCardValidateResponse{Provider: "Unknown", Validate: false, Message: "Credit card number is invalid"}},
		{CardNumber: "3529000000000009", Code: 400, Status: "BAD REQUEST", Data: web.CreditCardValidateResponse{Provider: "Unknown", Validate: false, Message: "Credit card number is invalid"}},
		{CardNumber: "6010000000000009", Code: 400, Status: "BAD REQUEST", Data: web.CreditCardValidateResponse{Provider: "Unknown", Validate: false, Message: "Credit card number is invalid"}},
		{CardNumber: "3528000000000009", Code: 400, Status: "BAD REQUEST", Data: web.CreditCardValidateResponse{Provider: "Unknown", Validate: false, Message: "Credit card number is invalid"}},
	}

	router := setupRouter()

	for _, tc := range testCases {
		request := httptest.NewRequest(http.MethodGet, "http://localhost:3000/api/credit-cards/"+tc.CardNumber, nil)
		request.Header.Add("Content-Type", "application/json")

		recorder := httptest.NewRecorder()

		router.ServeHTTP(recorder, request)

		response := recorder.Result()

		body, _ := io.ReadAll(response.Body)

		var responseBody map[string]interface{}
		json.Unmarshal(body, &responseBody)

		assert.Equal(t, tc.Code, int(responseBody["code"].(float64)))
		assert.Equal(t, tc.Status, responseBody["status"])
		assert.Equal(t, tc.Data.Provider, responseBody["data"].(map[string]interface{})["provider"])
		assert.Equal(t, tc.Data.Validate, responseBody["data"].(map[string]interface{})["validate"])
		assert.Equal(t, tc.Data.Message, responseBody["data"].(map[string]interface{})["message"])
	}
}
