package main

import (
	"net/http"

	"github.com/go-playground/validator/v10"
	"gitlab.com/taufiksty/credit-card-validator/app"
	"gitlab.com/taufiksty/credit-card-validator/controllers"
	"gitlab.com/taufiksty/credit-card-validator/helpers"
	"gitlab.com/taufiksty/credit-card-validator/services"
)

func main() {
	validate := validator.New()
	creditCardService := services.NewCreditCardService(validate)
	creditCardController := controllers.NewCreditCardController(creditCardService)
	router := app.NewRouter(creditCardController)

	server := http.Server{
		Addr:    "localhost:3000",
		Handler: router,
	}

	err := server.ListenAndServe()
	helpers.PanicIfError(err)
}
