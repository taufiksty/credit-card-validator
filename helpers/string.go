package helpers

func ReverseString(s string) string {
	var reversed string

	for i := len(s) - 1; i >= 0; i-- {
		reversed += string(s[i])
	}

	return reversed
}
