package controllers

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/taufiksty/credit-card-validator/exception"
	"gitlab.com/taufiksty/credit-card-validator/helpers"
	"gitlab.com/taufiksty/credit-card-validator/models/web"
	"gitlab.com/taufiksty/credit-card-validator/services"
)

type CreditCardControllerImpl struct {
	CreditCardService services.CreditCardService
}

func NewCreditCardController(creditCardService services.CreditCardService) CreditCardController {
	return &CreditCardControllerImpl{
		CreditCardService: creditCardService,
	}
}

func (controller *CreditCardControllerImpl) ValidateCreditCard(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	creditCardRequest := web.CreditCardValidateRequest{}
	creditCardRequest.CardNumber = params.ByName("cardNumber")

	creditCardResponse := controller.CreditCardService.ValidateCreditCard(request.Context(), creditCardRequest)

	if !creditCardResponse.Validate {
		panic(exception.NewClientError(creditCardResponse.Message, creditCardResponse.CardNumber))
	}

	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   creditCardResponse,
	}

	helpers.WriteToResponseBody(writer, webResponse)
}
