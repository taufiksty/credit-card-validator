package services

import (
	"context"

	"gitlab.com/taufiksty/credit-card-validator/models/web"
)

type CreditCardService interface {
	ValidateCreditCard(ctx context.Context, request web.CreditCardValidateRequest) web.CreditCardValidateResponse
}
