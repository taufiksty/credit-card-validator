package web

type CreditCardValidateResponse struct {
	CardNumber string `json:"card_number"`
	Provider   string `json:"provider"`
	Validate   bool   `json:"validate"`
	Message    string `json:"message"`
}
