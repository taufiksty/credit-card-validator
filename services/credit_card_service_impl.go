package services

import (
	"context"
	"regexp"
	"strconv"

	"github.com/go-playground/validator/v10"
	"gitlab.com/taufiksty/credit-card-validator/helpers"
	"gitlab.com/taufiksty/credit-card-validator/models/web"
)

type CreditCardServiceImpl struct {
	Validate *validator.Validate
}

func NewCreditCardService(validate *validator.Validate) CreditCardService {
	return &CreditCardServiceImpl{
		Validate: validate,
	}
}

func identifyProvider(cardNumber string) string {
	for pattern, provider := range helpers.CreditCardProviders {
		matched, _ := regexp.MatchString(pattern, cardNumber)

		if matched {
			return provider
		}
	}

	return "Unknown"
}

func (c *CreditCardServiceImpl) ValidateCreditCard(ctx context.Context, request web.CreditCardValidateRequest) web.CreditCardValidateResponse {
	reversed := helpers.ReverseString(request.CardNumber)
	sum := 0

	for i, digit := range reversed {
		digitInt, _ := strconv.Atoi(string(digit))

		if i%2 == 1 {
			doubled := digitInt * 2
			if doubled > 9 {
				doubled -= 9
			}

			sum += doubled
		} else {
			sum += digitInt
		}
	}

	isValid := sum%10 == 0

	var message string
	if !isValid {
		message = "Credit card number is invalid"
	} else {
		message = "Credit card number is valid"
	}

	return web.CreditCardValidateResponse{
		CardNumber: request.CardNumber,
		Provider:   identifyProvider(request.CardNumber),
		Validate:   isValid,
		Message:    message,
	}

}
