package exception

type ClientError struct {
	CardNumber string
	Error      string
}

func NewClientError(error, cardNumber string) ClientError {
	return ClientError{
		CardNumber: cardNumber,
		Error:      error,
	}
}
