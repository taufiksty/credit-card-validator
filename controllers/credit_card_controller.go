package controllers

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type CreditCardController interface {
	ValidateCreditCard(writer http.ResponseWriter, request *http.Request, parmas httprouter.Params)
}
